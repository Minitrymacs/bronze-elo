package logic;

public class Rechteck {
	protected Punkt punkt;
	protected double breite;
	protected double hoehe;

	public Rechteck() {
		this.punkt = new Punkt(0, 0);
		this.breite = 2;
		this.hoehe = 2;
	}

	public Rechteck(Punkt punkt, double breite, double hoehe) {
		super();
		this.punkt = punkt;
		this.breite = breite;
		this.hoehe = hoehe;
	}

	public Punkt getPunkt() {
		return punkt;
	}

	public void setPunkt(Punkt punkt) {
		this.punkt = punkt;
	}

	public double getBreite() {
		return breite;
	}

	public void setBreite(double breite) {
		this.breite = breite;
	}

	public double getHoehe() {
		return hoehe;
	}

	public void setHoehe(double hoehe) {
		this.hoehe = hoehe;
	}

	@Override
	public String toString() {
		return "Rechteck [punkt=" + punkt + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rechteck other = (Rechteck) obj;
		if (Double.doubleToLongBits(breite) != Double.doubleToLongBits(other.breite))
			return false;
		if (Double.doubleToLongBits(hoehe) != Double.doubleToLongBits(other.hoehe))
			return false;
		if (punkt == null) {
			if (other.punkt != null)
				return false;
		} else if (!punkt.equals(other.punkt))
			return false;
		return true;
	}

}
