package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class persönlicheDaten extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;

	/**
	 * Create the frame.
	 */
	public persönlicheDaten() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblNewLabel = new JLabel("Pers\u00F6nliche Daten:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblNewLabel, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(8, 2));
		
		JLabel lblNewLabel_21 = new JLabel("Nachname");
		lblNewLabel_21.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel.add(lblNewLabel_21);
		
		textField_6 = new JTextField();
		panel.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblNewLabel_20 = new JLabel("Vorname:");
		lblNewLabel_20.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel.add(lblNewLabel_20);
		
				textField_4 = new JTextField();
				panel.add(textField_4);
				textField_4.setColumns(10);
		
				JLabel lblNewLabel_1 = new JLabel("Voteanzahl:");
				lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panel.add(lblNewLabel_1);
		
				textField = new JTextField();
				panel.add(textField);
				textField.setColumns(10);
		
				JLabel lblNewLabel_7 = new JLabel("Geburtsdatum:");
				lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panel.add(lblNewLabel_7);
		
				textField_1 = new JTextField();
				textField_1.setEnabled(false);
				textField_1.setEditable(false);
				panel.add(textField_1);
				textField_1.setColumns(10);
		
				JLabel lblNewLabel_2 = new JLabel("Musikgeschmack:");
				lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panel.add(lblNewLabel_2);
		
		textField_3 = new JTextField();
		panel.add(textField_3);
		textField_3.setColumns(10);
		
				JLabel lblNewLabel_6 = new JLabel("Adresse:");
				lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panel.add(lblNewLabel_6);
		
				textField_5 = new JTextField();
				panel.add(textField_5);
				textField_5.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("");
		panel.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("");
		panel.add(lblNewLabel_4);
				
						JLabel lblNewLabel_5 = new JLabel("");
						panel.add(lblNewLabel_5);

		JLabel lblNewLabel_9 = new JLabel("");
		panel.add(lblNewLabel_9);

		JButton btnNewButton = new JButton("Zur\u00FCck");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				menue menue3 = new menue();
				menue3.setVisible(true);
			}
		});
		contentPane.add(btnNewButton, BorderLayout.SOUTH);

		JButton speichern = new JButton("SPEICHERN");
		contentPane.add(speichern, BorderLayout.EAST);
		
		speichern.addActionListener(new ActionListener() {
			

			public void actionPerformed(ActionEvent arg0) {
				try {
					Connection conn;
					String driver = "com.mysql.jdbc.Driver";
					String url = "jdbc:mysql://localhost:3306/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET";
					String username = "root";
					String password = "";
					Class.forName(driver);
					conn = DriverManager.getConnection(url, username, password);
				
					String query = " INSERT INTO gäste (nachname, vorname, voteanzahl, geburtsdatum, musikgeschmack, adresse)"
					        + " values (?, ?, ?, ?, ?, ?)";
					PreparedStatement preparedStmt = conn.prepareStatement(query);
					  preparedStmt.setString(1,textField_6.getText());
				      preparedStmt.setString(2, textField_4.getText());
				      preparedStmt.setString(3, textField.getText());
				      preparedStmt.setString(4, "0000-00-00");
				      preparedStmt.setString(5, textField_3.getText());
				      preparedStmt.setString(6, textField_5.getText());
				 
				      preparedStmt.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
					
				}
					
				});
		
				
	}
}

