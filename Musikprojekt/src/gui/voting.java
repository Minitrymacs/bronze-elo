package gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import javax.swing.JTextArea;

public class voting extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private Connection con;
	String username = "root";
	String password = "";

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public voting(String nameOfTable) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Voting:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblNewLabel, BorderLayout.NORTH);

		JButton btnNewButton = new JButton("VOTEN");
		btnNewButton.setEnabled(false);
		contentPane.add(btnNewButton, BorderLayout.EAST);
		
		btnNewButton.addActionListener(new ActionListener() {
			

			public void actionPerformed(ActionEvent arg0) {
				try {
					Connection conn;
					String driver = "com.mysql.jdbc.Driver";
					String url = "jdbc:mysql://localhost:3306/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET";
					String username = "root";
					String password = "";
					Class.forName(driver);
					conn = DriverManager.getConnection(url, username, password);
				
					String query = " INSERT INTO vote (f_nachname, f_voteanzahl, votes_id)"
					        + " values (?, ?, ?)";
					PreparedStatement preparedStmt = conn.prepareStatement(query);
					  preparedStmt.setString(1,textField.getText());
				   
				 
				      preparedStmt.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
					
				}
					
				});
		
		JButton btnNewButton_1 = new JButton("zur\u00FCck");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				menue menue3 = new menue();
				menue3.setVisible(true);

			}
		});
		contentPane.add(btnNewButton_1, BorderLayout.SOUTH);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(8, 2));

		JLabel lblNewLabel_5 = new JLabel("");
		panel.add(lblNewLabel_5);

		JLabel lblNewLabel_4 = new JLabel("");
		panel.add(lblNewLabel_4);

		JLabel lblNewLabel_3 = new JLabel("");
		panel.add(lblNewLabel_3);

		JLabel lblNewLabel_2 = new JLabel("");
		panel.add(lblNewLabel_2);

		JLabel lblNewLabel_6 = new JLabel("");
		panel.add(lblNewLabel_6);

		JLabel lblNewLabel_7 = new JLabel("");
		panel.add(lblNewLabel_7);

		JLabel lblNewLabel_8 = new JLabel(" Liednummer:");
		panel.add(lblNewLabel_8);

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(textField);
		textField.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("");
		panel.add(lblNewLabel_1);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.WEST);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));

		JTextArea txtrmusikliste = new JTextArea();
		txtrmusikliste.setTabSize(4);
		txtrmusikliste.setEditable(false);
		panel_1.add(txtrmusikliste);
		
		try {
			// Verbindung herstellen
			createConnection();
			// Ergebnisse anzeigen.
			String query = "SELECT titelname, bandname  FROM " + nameOfTable;
			java.sql.Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int columns = rs.getMetaData().getColumnCount();
			for (int i = 1; i <= columns; i++)
				txtrmusikliste.append(rs.getMetaData().getColumnLabel(i) + "\t\t");
			txtrmusikliste.append("\n");
			txtrmusikliste.append("\n");
			while (rs.next()) {
				for (int i = 1; i <= columns; i++) {
					txtrmusikliste.append(rs.getString(i) + "\t\t\t");
				}
				txtrmusikliste.append("\n");
			}
			rs.close();
			stmt.close();
			// Verbindung schlie�en
			closeProgramm();
		} catch (SQLException e) {
			System.err.println("SQL Fehler - " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Der Datenbank treiber wurde nicht gefunden. - " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	public void closeProgramm() throws SQLException {
		this.con.close();
	}

	
public void createConnection() throws SQLException, ClassNotFoundException {
	// Treiber initialisieren
	Class.forName(DRIVER);
	// Uri f�r die Verbindung zu der Datenbank
	String mySqlUrl = "jdbc:mysql://localhost:3306/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET";
	// Verbindung herstellen.
	con = DriverManager.getConnection(mySqlUrl, username, password);
}
}
