package gui;

import java.awt.BorderLayout;
import java.awt.Label;
import java.awt.TextArea;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class playlist extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private JPanel contentPane;
	private Connection con;
	String username = "root";
	String password = "";

	/**
	 * Launch the application.
	 */
	public void createConnection() throws SQLException, ClassNotFoundException {
		// Treiber initialisieren
		Class.forName(DRIVER);
		// Uri f�r die Verbindung zu der Datenbank
		String mySqlUrl = "jdbc:mysql://localhost:3306/musikvoting?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=CET";
		// Verbindung herstellen.
		con = DriverManager.getConnection(mySqlUrl, username, password);
	}


	/**
	 * Create the frame.
	 */
	public playlist(String nameOfTable) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JButton btnNewButton = new JButton("zur\u00FCck");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				menue menue3 = new menue();
				menue3.setVisible(true);
			}
		});
		contentPane.add(btnNewButton, BorderLayout.SOUTH);

		TextArea textArea = new TextArea();
		try

		{

		} catch (Exception e) {
			e.printStackTrace();
		}
		contentPane.add(textArea, BorderLayout.CENTER);
		try {
			// Verbindung herstellen
			createConnection();
			// Ergebnisse anzeigen.
			String query = "SELECT * FROM " + nameOfTable;
			java.sql.Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int columns = rs.getMetaData().getColumnCount();
			textArea.append("Alle Musikeintr�ge ");
			textArea.append("\n");
			textArea.append("\n");
			for (int i = 1; i <= columns; i++)
				textArea.append(rs.getMetaData().getColumnLabel(i) + "\t\t");
			textArea.append("\n");
			textArea.append("\n");
			while (rs.next()) {
				for (int i = 1; i <= columns; i++) {
					textArea.append(rs.getString(i) + "\t\t\t");
				}
				textArea.append("\n");
			}
			rs.close();
			stmt.close();
			// Verbindung schlie�en
			closeProgramm();
		} catch (SQLException e) {
			System.err.println("SQL Fehler - " + e.getLocalizedMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Der Datenbank treiber wurde nicht gefunden. - " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	public void closeProgramm() throws SQLException {
		this.con.close();
	}

}
